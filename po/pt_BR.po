# Portuguese (BR) translation for DevilBlanket.
# Copyright (C) 2020 Rafael Mardojai CM
# This file is distributed under the same license as the devilblanket package.
# Pedro Fleck <pedrohk@ufcspa.edu.br>, 2020-2021.
#
msgid ""
msgstr ""
"Project-Id-Version: devilblanket\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-09-17 17:21-0500\n"
"PO-Revision-Date: 2021-03-27 13:49-0300\n"
"Last-Translator: Pedro Fleck <pedrohk@ufcspa.edu.br>\n"
"Language-Team: Brazilian Portuguese <pedrohk@ufcspa.edu.br>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 3.38.0\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"

#: data/com.rafaelmardojai.DevilBlanket.desktop.in:3
#: data/com.rafaelmardojai.DevilBlanket.metainfo.xml.in:6
#: data/resources/window.ui:19 devilblanket/main.py:31 devilblanket/window.py:260
#: devilblanket/mpris.py:141
msgid "DevilBlanket"
msgstr "DevilBlanket"

#: data/com.rafaelmardojai.DevilBlanket.desktop.in:4
#: data/com.rafaelmardojai.DevilBlanket.metainfo.xml.in:7 data/resources/about.ui:10
#: devilblanket/mpris.py:136 devilblanket/mpris.py:139
msgid "Listen to different sounds"
msgstr "Escute diferentes sons"

#. Translators: These are search terms to find this application. Do NOT translate or localize the semicolons. The list MUST also end with a semicolon.
#: data/com.rafaelmardojai.DevilBlanket.desktop.in:10
msgid "Concentrate;Focus;Noise;Productivity;Sleep;"
msgstr "Concentração;Foco;Ruído;Produtividade;Sono;"

#: data/com.rafaelmardojai.DevilBlanket.metainfo.xml.in:9
msgid ""
"Improve focus and increase your productivity by listening to different "
"sounds. Or allows you to fall asleep in a noisy environment."
msgstr ""
"Melhore seu foco e aumente sua produtividade ouvindo diferentes sons. Ou "
"permita-se cair no sono em um ambiente ruidoso."

#: data/com.rafaelmardojai.DevilBlanket.metainfo.xml.in:17
msgid "Rafael Mardojai CM"
msgstr "Rafael Mardojai C. M."

#. Translators: Keep single quote please!
#: data/com.rafaelmardojai.DevilBlanket.gschema.xml:42
msgctxt "visible-name"
msgid "'Default'"
msgstr ""

#: data/resources/window.ui:139 data/resources/window.ui:260
msgid "Saved Presets"
msgstr ""

#: data/resources/window.ui:148 data/resources/window.ui:275
msgid "Add Preset..."
msgstr ""

#: data/resources/window.ui:157
msgid "Reset Volume Levels"
msgstr ""

#: data/resources/window.ui:174
#, fuzzy
msgid "Keep Playing When Closed"
msgstr "Continuar reproduzindo ao fechar"

#: data/resources/window.ui:191
msgid "Preferences"
msgstr "Preferências"

#: data/resources/window.ui:200
msgid "Keyboard Shortcuts"
msgstr "Atalhos de teclado"

#: data/resources/window.ui:209
msgid "About DevilBlanket"
msgstr "Sobre o DevilBlanket"

#: data/resources/window.ui:236 data/resources/shortcuts.ui:39
msgid "Quit"
msgstr "Sair"

#: data/resources/preferences.ui:16
msgid "Appearance"
msgstr ""

#: data/resources/preferences.ui:20
msgid "Dark Mode"
msgstr ""

#: data/resources/preferences.ui:36
msgid "Behavior"
msgstr "Comportamento"

#: data/resources/preferences.ui:40
msgid "Autostart in background"
msgstr "Iniciar automaticamente em segundo plano"

#: data/resources/preset-dialog.ui:25
msgid "Cancel"
msgstr ""

#: data/resources/preset-dialog.ui:66
msgid "Preset Name"
msgstr ""

#: data/resources/preset-row.ui:39
msgid "Delete Preset"
msgstr ""

#: data/resources/preset-row.ui:61
msgid "Rename Preset"
msgstr ""

#: data/resources/about.ui:11
msgid "Copyright 2020-2021 Rafael Mardojai CM"
msgstr "Direitos autorais 2020-2021 Rafael Mardojai CM"

#. Replace me with your names, You can make a mailto link: translator-name <user@domain.org>
#: data/resources/about.ui:15
msgctxt "Add your names here, do not remove previous names!"
msgid "translator-credits"
msgstr ""
"Pedro Fleck <freckts@gmail.com>, 2021\n"
"Gustavo Costa <gusta@null.net>"

#: data/resources/shortcuts.ui:13
msgid "General"
msgstr "Geral"

#: data/resources/shortcuts.ui:18
msgid "Play/Pause sounds"
msgstr "Reproduzir/pausar sons"

#: data/resources/shortcuts.ui:25
msgid "Add custom sound"
msgstr "Adicionar som personalizado"

#: data/resources/shortcuts.ui:32
msgid "Close window"
msgstr "Fechar a janela"

#: devilblanket/window.py:16
msgid "Nature"
msgstr "Natureza"

#: devilblanket/window.py:20 devilblanket/about.py:17 devilblanket/about.py:35
msgid "Rain"
msgstr "Chuva"

#: devilblanket/window.py:24 devilblanket/about.py:18 devilblanket/about.py:35
msgid "Storm"
msgstr "Tempestade"

#: devilblanket/window.py:28 devilblanket/about.py:29 devilblanket/about.py:35
msgid "Wind"
msgstr "Vento"

#: devilblanket/window.py:32 devilblanket/about.py:26 devilblanket/about.py:35
msgid "Waves"
msgstr "Ondas"

#: devilblanket/window.py:36 devilblanket/about.py:22
msgid "Stream"
msgstr "Córrego"

#: devilblanket/window.py:40 devilblanket/about.py:24 devilblanket/about.py:35
msgid "Birds"
msgstr "Pássaros"

#: devilblanket/window.py:44 devilblanket/about.py:25
msgid "Summer Night"
msgstr "Noite de verão"

#: devilblanket/window.py:49
msgid "Travel"
msgstr "Viagem"

#: devilblanket/window.py:53 devilblanket/about.py:30
msgid "Train"
msgstr "Trem"

#: devilblanket/window.py:57 devilblanket/about.py:20 devilblanket/about.py:35
msgid "Boat"
msgstr "Barco"

#: devilblanket/window.py:61 devilblanket/about.py:21 devilblanket/about.py:35
msgid "City"
msgstr "Cidade"

#: devilblanket/window.py:66
msgid "Interiors"
msgstr "Interiores"

#: devilblanket/window.py:70 devilblanket/about.py:27
msgid "Coffee Shop"
msgstr "Cafeteria"

#: devilblanket/window.py:74 devilblanket/about.py:19
msgid "Fireplace"
msgstr "Lareira"

#: devilblanket/window.py:79
msgid "Noise"
msgstr "Ruído"

#: devilblanket/window.py:83 devilblanket/about.py:28
msgid "Pink Noise"
msgstr "Ruído rosa"

#: devilblanket/window.py:87 devilblanket/about.py:23
msgid "White Noise"
msgstr "Ruído branco"

#. Setup user custom sounds
#: devilblanket/window.py:196
msgid "Custom"
msgstr "Personalizado"

#: devilblanket/window.py:218
msgid "Open audio"
msgstr "Abrir áudio"

#: devilblanket/preferences.py:62
#, fuzzy
msgid "Autostart DevilBlanket in background."
msgstr "Iniciar DevilBlanket em segundo plano."

#: devilblanket/preferences.py:90 devilblanket/preferences.py:127
msgid "Request error"
msgstr "Erro de solicitação"

#: devilblanket/preferences.py:93 devilblanket/preferences.py:130
msgid "The autostart request failed."
msgstr "A solicitação para rodar em segundo plano falhou."

#: devilblanket/preferences.py:115
msgid "Authorization failed"
msgstr "Autorização falhou"

#: devilblanket/preferences.py:118
msgid ""
"Make sure DevilBlanket has permission to run in \n"
"the background in Settings → Applications → \n"
"DevilBlanket and try again."
msgstr ""
"Certifique-se de que o DevilBlanket tem permissão \n"
"para rodar em segundo plano em Configurações → \n"
"Aplicativos → DevilBlanket e tente novamente."

#: devilblanket/presets.py:112
msgid "Add Preset"
msgstr ""

#: devilblanket/presets.py:113
msgid "Create"
msgstr ""

#: devilblanket/presets.py:117
msgid "Edit Preset"
msgstr ""

#: devilblanket/presets.py:119
msgid "Save"
msgstr ""

#: devilblanket/about.py:12
msgid "Sounds icons"
msgstr "Ícones dos sons"

#: devilblanket/about.py:13
msgid "App icon"
msgstr "Ícone do aplicativo"

#. Set sound artists list
#: devilblanket/about.py:60
msgid "Sounds by"
msgstr "Sons por"

#. Set sound editors list
#: devilblanket/about.py:62
msgid "Sounds edited by"
msgstr "Sons editados por"

#~ msgid "Unknown"
#~ msgstr "Desconhecido"
